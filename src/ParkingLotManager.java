import java.util.Random;
import java.util.Scanner;


public class ParkingLotManager {
    private boolean isOn;
    public ParkingLotManager() {
        this.isOn = true;
    }

    public boolean isOn() {
        return isOn;
    }

    public void setOn(boolean on) {
        isOn = on;
    }


    public char Instructions() {
        System.out.println("Commands:");
        System.out.println(" ");;
        System.out.println("Continue");
        System.out.println("Stop");
        System.out.println("status");
        System.out.println("clear ");


        return 0;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of Parking Slots: ");
        int parkingSlots = scanner.nextInt();
        Parking parking = new Parking(parkingSlots);
        ParkingLotManager Manager = new ParkingLotManager();
        System.out.println(Manager.Instructions());
        Random random = new Random();
        int id = 1000;
        while(Manager.isOn()){
            parking.updateStatus();
            switch(scanner.nextLine()){

                case("clear"):{
                    parking.clearParking();
                    System.out.println("Parking is clear");
                    parking.RoundEnding();
                    break;
                }
                case("stop"):{
                    Manager.setOn(false);
                    break;
                }
                case("status"):{
                    int x = parking.getParkingSlots();
                    int y = parking.getEmptySlots();
                    System.out.println("There are " + x + " parking places");
                    System.out.println("Empty slots " + y);
                    System.out.println("Slots taken " + (x-y));
                    if (y == 0) {
                        System.out.println("There will be empty slots in " + parking.getWhenSlotsBeEmpty() + " steps");
                    }
                    break;
                }
                case("continue"):{
                    Car car = new Car(id);
                    if(parking.getEmptySlots() > 0) {
                        int steps = random.nextInt(10) + 1;
                        parking.CarParking(car, steps);
                        System.out.println("Car with ID " + id + " in " + car.getSlot() + " slot for " + car.getTimeForCar() + " steps");
                    }
                    else{
                        System.out.println("Sorry, but there are no free Parking spaces.");
                        System.out.println("You should wait " + parking.getWhenSlotsBeEmpty()+ " steps until parking places are available ");
                    }
                    id++;
                    parking.RoundEnding();
                    break;
                }
            }
        }
    }
}


