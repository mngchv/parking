public class Car {
    private final int carID;
    private int carTime;
    private int slot;



    @Override
    public String toString() {
        return "Car with ID " + carID +
                " and time for parking " + carTime + " times.";

    }

    public Car(int carID) {
        this.carID = carID;
        this.carTime = 0;
        this.slot = 0;
    }

    public int getSlot() {
        return slot;
    }

    public int getTimeForCar() {
        return carTime;
    }

    public void setSlot(int place) {
        this.slot = place;
    }

    public void setTimeForCar(int timeForCar) {
        this.carTime = timeForCar;
    }

}