public class Parking {
    public int ParkingSlots;
    public int EmptySlots;
    public int WhenSlotBeEmpty;
    public Car[] Slots;


    public Parking(int ParkingSlots){
        this.ParkingSlots = ParkingSlots;
        this.EmptySlots = ParkingSlots;
        this.WhenSlotBeEmpty = 0;
        this.Slots = new Car[ParkingSlots];

    }

    @Override
    public String toString() {
        return "Parking with "+ ParkingSlots + " parking slots.";
    }

    public int getParkingSlots() {
        return ParkingSlots;
    }

    public int getEmptySlots() {
        return EmptySlots;
    }

    public int getWhenSlotsBeEmpty() {
        return WhenSlotBeEmpty;
    }

    public void setEmptySlots(int emptySlots) {
        EmptySlots = emptySlots;
    }

    public void setWhenSlotsBeEmpty(int whenSlotsBeEmpty) {
        WhenSlotBeEmpty = whenSlotsBeEmpty;
    }

    public void clearParking(){
        for(int j = 0; j < ParkingSlots; j++){
            if(Slots[j] != null) {
                Slots[j].setTimeForCar(0);
                Slots[j].setSlot(0);
                Slots[j] = null;
            }
        }
    }
    public int returnEmptySlot(){
        for(int j = 0; j < getParkingSlots(); j++){
            if(Slots[j] == null){
                return j;
            }
        }
        return 0;
    }
    public void CarParking(Car car, int timeForCar){
        int k = returnEmptySlot();
        Slots[k] = car;
        Slots[k].setTimeForCar(timeForCar);
        Slots[k].setSlot(k+1);
    }

    public void RoundEnding(){
        for(int j = 0; j < ParkingSlots; j++) {
            if(Slots[j] != null){
                Slots[j].setTimeForCar(Slots[j].getTimeForCar()-1);
            }
        }
    }

    public void updateStatus(){
        int minAvailable = 10;
        int places = 0;
        for(int j = 0; j < ParkingSlots; j++){
            if(Slots[j] != null){
                int b = Slots[j].getTimeForCar();
                if(b==0){
                    Slots[j].setSlot(0);
                    Slots[j] = null;
                }
                else if(b <= minAvailable){
                    minAvailable = b;
                }
            }
            else{
                places++;
            }
        }
        if (places != 0){
            setWhenSlotsBeEmpty(0);
        }
        else{
            setWhenSlotsBeEmpty(minAvailable);
        }
        setEmptySlots(places);
    }
}
